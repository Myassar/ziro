@extends('layouts.app')

@section('content')
<!-- <task-component></task-component> -->
<div class="container">
  @auth
     
     <router-view></router-view>
@endauth
</div>

@endsection
