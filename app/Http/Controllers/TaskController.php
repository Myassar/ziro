<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Repositories\TaskRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\StoreTask;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{

  protected $task,$user;

  /**
   * [__construct description]
   * @param TaskRepository $task    [description]
   * @param Request        $request [description]
   */
    public function __construct(TaskRepository $task,UserRepository $user,Request $request)
    {
      $this->task = $task;
      $this->user =$user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tasks = $this->task->getAll(auth()->user()->id);
      $users=$this->user->getAll();
      $data['tasks']=$tasks;
      $data['users']=$users;

      return response()->json(['result'=> true, 'data'=>$data,'message'=> '']);
    }

    /**
     * share task with another user.
     *
     * @return \Illuminate\Http\Response
     */
    public function shareTaskWith(Task $task,$user_id)
    {
          $tasks = $this->task->shareTaskWith($task,$user_id);
          return response()->json(['result'=> true, 'data'=>'','message'=> 'shared completed Successfully']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'title' => 'required|string|max:191',
        'body' => 'required|string',
     ]);

     if ($validator->fails()) {
            return response()->json(['result'=> false, 'data'=>'','message'=> $validator->messages()->first()]);
     }


     $request->merge(['user_id' => auth()->user()->id]);
     // dd($request->all());
      if ($this->task->create($request->all())) {
          return response()->json(['result'=> true, 'data'=>'','message'=> 'Task Added Successfully']);
      } else {
         return response()->json(['result'=> false, 'data'=>'','message'=> 'Task Added failed']);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(task $task)
    {
        return response()->json(['result'=> true, 'data'=>$task,'message'=> '']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $validator = Validator::make($request->all(), [
          'title' => 'required|string|max:191',
          'body' => 'required|string',
       ]);

       if ($validator->fails()) {
              return response()->json(['result'=> false, 'data'=>'','message'=> $validator->messages()->first()]);
       }


       $request->merge(['user_id' => auth()->user()->id]);

      if ($this->task->update($request->all(), $task)) {
          return response()->json(['result'=> true, 'data'=>'','message'=> 'Task updated Successfully']);
      } else {
          return response()->json(['result'=> false, 'data'=>'','message'=> 'Task updated failed']);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($task)
    {

      if ($Task=$this->task->getByIdAndCheckOwner($task,auth()->user()->id)) {
            if ($this->task->delete($Task)) {
            return response()->json(array('success' => true, 'data' => '', 'message' => 'Task Deleted Successflly'));
            }
          return response()->json(array('success' => false, 'data' => '', 'message' => 'Task Deleted Failed '));
      } else {
          return response()->json(array('success' => false, 'data' => '', 'message' => 'Task Deleted Failed '));
      }
    }

    public function completeTask(Task $task)
    {
        $this->task->completeTask($task);
        return response()->json(array('success' => true, 'data' => '', 'message' => 'Task Completed Successfully'));
    }
}
