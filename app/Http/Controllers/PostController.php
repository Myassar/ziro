<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use App\Http\Repositories\PostRepository;
use App\Http\Requests\StorePost;

class PostController extends Controller
{
  protected $post;

  /**
   * [__construct description]
   * @param AreaRepository $area    [description]
   * @param Request        $request [description]
   */
    public function __construct(PostRepository $post,Request $request)
    {
      $this->post = $post;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $posts = $this->post->getAll();
          return response()->json(['result'=> true, 'data'=>$posts,'message'=> '']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
      if ($this->post->create($request->all())) {
          return response()->json(['result'=> true, 'data'=>'','message'=> 'Post Added Successfully']);
      } else {
         return response()->json(['result'=> false, 'data'=>'','message'=> 'Post Added failed']);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, post $post)
    {
      if ($this->post->update($request->all(), $post)) {
          return response()->json(['result'=> true, 'data'=>'','message'=> 'Post updated Successfully']);
      } else {
          return response()->json(['result'=> false, 'data'=>'','message'=> 'Post updated failed']);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
      if ($post->delete()) {
          return response()->json(array('success' => true, 'data' => '', 'message' => 'Post Deleted Successflly'));
      } else {
          return response()->json(array('success' => false, 'data' => '', 'message' => 'Post Deleted Failed '));
      }
    }
}
