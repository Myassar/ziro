<?php

namespace App\Http\Repositories;

use App\Task;
use Config;


class TaskRepository
{
  protected $lang;
  function __construct()
  {
    $this->lang=Config::get('translatable.locales');
  }


  public function getAll($user_id)
  {

    return Task::leftJoin('user_tasks', 'tasks.id', '=', 'user_tasks.task_id')
                 ->where([['tasks.complete',0],['tasks.user_id',$user_id]])
                 ->orWhere([['tasks.complete',0],['user_tasks.user_id',$user_id]])
                 ->select('tasks.*')
                 ->get();

  }

  public function getdeletedList()
  {

      return  Task::onlyTrashed()->get();

  }

  public function shareTaskWith($task,$user_id)
  {
    $task->User()->attach($user_id);
    return true;
  }

  public function getById($id)
  {

      return  Task::find($id);

  }

  public function getByIdAndCheckOwner($id,$user_id)
  {
        return  Task::where([['id',$id],['user_id',$user_id]])->first();
  }

  public function getTrashedById($id)
  {

      return Task::onlyTrashed()->where('id',$id)->first();

  }


 public function create($data)
 {

   if ($Task=Task::create($data)) {

      return $Task;
   }
   return false;
 }

 public function update($data,$Task)
 {
   if ($Task->update($data)) {
      return $Task;
   }
    return false;
 }

 public function completeTask($Task)
 {
   $Task->complete=1;
   $Task->save();
    return $Task;
 }

 public function delete($Task)
 {
   if ($Task->delete()) {
     return true;
   }

    return false;

 }

 public function restore($Task)
 {
   if ($Task->restore()) {
     return true;
   }

    return false;

 }


}
