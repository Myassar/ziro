<?php

namespace App\Http\Repositories;

use App\User;
use Config;


class UserRepository
{
  protected $lang;
  function __construct()
  {
    $this->lang=Config::get('translatable.locales');
  }


  public function getAll()
  {

    return User::get();

  }



}
