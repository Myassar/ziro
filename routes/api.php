<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});






Route::group(['middleware' => ['auth:api']], function () {
  Route::post('/add-task', 'TaskController@store');
  Route::post('/task/update/{task}', 'TaskController@update');
  Route::delete('/task/delete/{task}', 'TaskController@destroy');
  Route::get('/all-tasks', 'TaskController@index');
  Route::get('/task/{task}', 'TaskController@edit');
  Route::put('/task/complete/{task}', 'TaskController@completeTask');
  Route::post('/task/shareWith/{task}/{user_id}', 'TaskController@shareTaskWith');
});
